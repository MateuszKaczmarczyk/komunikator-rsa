package pl.edu.uwm.od.klient.szyfrowanie;

import java.math.BigInteger;
import java.util.Random;

public class RSA {

	private BigInteger p;// losowa liczba pierwsza: p
	private BigInteger q;// losowa liczba pierwsza: q
	private BigInteger n;// wynik mno�enia p*q
	private BigInteger phi;// (p-1)(q-1)
	private BigInteger e;
	private BigInteger d;// (e^-1 mod phi).
	private int bitlength = 1024;

	private Random r;

	public RSA() {
		r = new Random();
		p = BigInteger.probablePrime(bitlength, r);
		q = BigInteger.probablePrime(bitlength, r);
		n = p.multiply(q);

		phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
		e = BigInteger.probablePrime(bitlength / 2, r);

		while (phi.gcd(e).compareTo(BigInteger.ONE) > 0 && e.compareTo(phi) < 0) {
			e.add(BigInteger.ONE);
		}

		d = e.modInverse(phi);
	}

	public RSA(BigInteger e, BigInteger d, BigInteger n) {
		this.e = e;
		this.d = d;
		this.n = n;
	}

	/*
	 * Konstruktor s�u�y do inicjalizacji obiektu RSA je�li znamy tylko e i n
	 * (klucz publiczny). Wystarczy do zaszyfrowania danych.
	 */
	public RSA(BigInteger e, BigInteger n) {
		this.e = e;
		this.n = n;
	}

	public byte[] zaszyfruj(byte[] message) {
		System.out.println("Szyfruje wiadomosc: " + new String(message) + " kluczem " + e + " | " + d + " | " + n);
		return (new BigInteger(message)).modPow(e, n).toByteArray();
	}

	public byte[] odszyfruj(byte[] message) {
		System.out.println("Odszyfrowuje wiadomosc kluczem " + e + " | " + d + " | " + n);
		return (new BigInteger(message)).modPow(d, n).toByteArray();
	}

	public String podajKluczPubliczny() {
		return n + ":" + e;
	}

}
