package pl.edu.uwm.od.klient.kontroler;

import java.io.DataOutputStream;
import java.math.BigInteger;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import pl.edu.uwm.od.klient.komunikacja.SocketClient;
import pl.edu.uwm.od.klient.szyfrowanie.RSA;

public class CzatController {
	private static final Logger log = LoggerFactory.getLogger(CzatController.class);

	@FXML
	private TextField wiadomoscField;
	@FXML
	private Label czatLabel;

	SocketClient socketClient;

	RSA rsa;

	public CzatController() {
		this.rsa = new RSA();
		this.socketClient = new SocketClient(this, this.rsa);

		try {
			DataOutputStream dOut = new DataOutputStream(socketClient.getSocket().getOutputStream());
			System.out.println("Wysylam zadanie o handshake: " + podajHandshake());
			dOut.writeInt(podajHandshake().getBytes().length);
			dOut.write(podajHandshake().getBytes());
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	private String podajHandshake() {
		return "handshake:" + rsa.podajKluczPubliczny();
	}

	public void wyslij() {

		String wiadomosc = wiadomoscField.getText();

		StringBuilder builder = new StringBuilder();

		if (!StringUtils.isEmpty(wiadomosc)) {
			builder.append(wiadomosc);
		}

		if (builder.length() > 0) {
			log.debug("Wysy�am wiadomo�� na serwer.");
			String[] kluczPubliczny = socketClient.getKluczPublicznySerwera().split(":");
			wyslijWiadomosc(builder.toString(), kluczPubliczny);
		} else {
			log.debug("Nie mo�na wys�a� pustej wiadomo�ci.");
		}
	}

	private void wyslijWiadomosc(String wiadomosc, String[] kluczPubliczny) {
		try {
			DataOutputStream dOut = new DataOutputStream(socketClient.getSocket().getOutputStream());

			RSA rsa = new RSA(new BigInteger(kluczPubliczny[1]), new BigInteger(kluczPubliczny[0]));
			byte[] zaszyfrowane = rsa.zaszyfruj(wiadomosc.getBytes());
			dOut.writeInt(zaszyfrowane.length);
			dOut.write(zaszyfrowane);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void dodajTekstDoCzatu(String val) {
		Platform.runLater(() -> {
			String prevText = this.czatLabel.getText();
			this.czatLabel.setText(prevText + " \n" + val);
		});
	}

}
