package pl.edu.uwm.od.klient.komunikacja;

import java.io.DataInputStream;
import java.io.PrintWriter;
import java.net.Socket;

import pl.edu.uwm.od.klient.kontroler.CzatController;
import pl.edu.uwm.od.klient.szyfrowanie.RSA;

public class SocketClient {

	private Socket socket;
	private PrintWriter printWriter;
	private String kluczPublicznySerwera;
	private CzatController helloController;

	public SocketClient(CzatController helloController, RSA rsa) {
		this.helloController = helloController;
		try {
			this.socket = new Socket("localhost", 2000);
			this.printWriter = new PrintWriter(socket.getOutputStream(), true);

			DataInputStream in = new DataInputStream(socket.getInputStream());
			ListenToServerResponseThread listener = new ListenToServerResponseThread(this, in, rsa);
			listener.start();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public PrintWriter getPrintWriter() {
		return printWriter;
	}

	public void setPrintWriter(PrintWriter printWriter) {
		this.printWriter = printWriter;
	}

	public String getKluczPublicznySerwera() {
		return kluczPublicznySerwera;
	}

	public void setKluczPublicznySerwera(String kluczPublicznySerwera) {
		this.kluczPublicznySerwera = kluczPublicznySerwera;
	}

	public void wyswietlWiadomoscNaLiscie(String txt) {
		this.helloController.dodajTekstDoCzatu(txt);
	}

}
