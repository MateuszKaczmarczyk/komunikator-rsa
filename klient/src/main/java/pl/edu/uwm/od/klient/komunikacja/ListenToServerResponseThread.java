package pl.edu.uwm.od.klient.komunikacja;

import java.io.DataInputStream;

import pl.edu.uwm.od.klient.szyfrowanie.RSA;

public class ListenToServerResponseThread extends Thread {

	private DataInputStream in;
	private RSA rsa;
	private SocketClient socketClient;

	public ListenToServerResponseThread(SocketClient socketClient, DataInputStream in, RSA rsa) {
		this.socketClient = socketClient;
		this.in = in;
		this.rsa = rsa;
	}

	public void run() {
		try {
			System.out.println("Nas�uchuj� na odpowied� z serwera.");

			while (true) {
				int length = in.readInt();
				if (length > 0) {
					System.out.println("Odebra�em wiadomo�� z serwera");
					byte[] message = new byte[length];
					in.readFully(message, 0, message.length);

					if (this.socketClient.getKluczPublicznySerwera() == null) {
						String messageStr = new String(message);
						messageStr = messageStr.replace("handshake:", "");
						this.socketClient.setKluczPublicznySerwera(messageStr);
					} else {
						// System.out.println("Surowa wiadomo��:" +
						// bytesToString(message));
						// System.out.println("Odszyfrowana wiadomo��:" + new
						// String(rsa.odszyfruj(message)));
						socketClient.wyswietlWiadomoscNaLiscie("Surowa wiadomosc:" + bytesToString(message));
						socketClient.wyswietlWiadomoscNaLiscie(
								"Odszyfrowana wiadomosc:" + new String(rsa.odszyfruj(message)));
					}
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static String bytesToString(byte[] encrypted) {
		String test = "";
		for (byte b : encrypted) {
			test += Byte.toString(b);
		}
		return test;
	}
}
