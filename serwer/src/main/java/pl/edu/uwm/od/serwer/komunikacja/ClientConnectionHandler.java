package pl.edu.uwm.od.serwer.komunikacja;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.util.UUID;

import pl.edu.uwm.od.serwer.szyfrowanie.RSA;

public class ClientConnectionHandler implements Runnable {

	private Socket socket;
	private String nazwa;
	private String kluczPubliczny; // klucz publiczny danego klienta

	public ClientConnectionHandler(Socket socket) {
		this.socket = socket;
		this.nazwa = generujNazweKlienta();
	}

	private String generujNazweKlienta() {
		return UUID.randomUUID().toString().substring(0, 6);
	}

	@Override
	public void run() {
		try {
			System.out.println("Podłączono nowego klienta: " + this.nazwa);

			while (true) {
				DataInputStream in = new DataInputStream(socket.getInputStream());
				int length = in.readInt();
				if (length > 0) {
					System.out.println("Odebrałem wiadomość od klienta");

					byte[] message = new byte[length];
					in.readFully(message, 0, message.length);

					if (this.kluczPubliczny == null) {
						System.out.println("Zapisuje klucz publiczny klienta");
						String messageStr = new String(message);
						messageStr = messageStr.replace("handshake:", "");

						// Klucz publiczny w formie n:e
						this.kluczPubliczny = messageStr;

						// Wysłanie klucza publicznego serwera,
						// w ramach wymiany "handshake"
						wyslijKluczPubliczny(SocketServer.podajKluczPublicznySerwera());
					} else {
						// Rozesłanie (broadcast) wiadomości.
						SocketServer.rozeslijWiadomosc(nazwa, message);
					}
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void wyslijWiadomosc(String autor, String wiadomosc, String[] kluczPubliczny) {
		try {
			DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());

			RSA rsa = new RSA(new BigInteger(kluczPubliczny[1]), new BigInteger(kluczPubliczny[0]));
			byte[] zaszyfrowane = rsa.zaszyfruj(wiadomosc.getBytes());
			dOut.writeInt(zaszyfrowane.length);
			dOut.write(zaszyfrowane);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void wyslijKluczPubliczny(String kluczPubliczny) {
		try {
			DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());
			dOut.writeInt(kluczPubliczny.length());
			dOut.write(kluczPubliczny.getBytes());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public String podajKluczPubliczny() {
		return kluczPubliczny;
	}

}
