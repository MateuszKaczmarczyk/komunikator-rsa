package pl.edu.uwm.od.serwer.komunikacja;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import pl.edu.uwm.od.serwer.szyfrowanie.RSA;

/**
 * Serwer nasłuchujący wiadomości i broadcastujący je do wszystkich podłączonych
 * klientów.
 */
public class SocketServer {

	private static List<ClientConnectionHandler> listaKlientow = new ArrayList<>();
	private static String kluczPubliczny;
	private static RSA rsa;

	public static void main(String[] args) {

		try {
			ServerSocket serverSocket = new ServerSocket(2000);

			rsa = new RSA();
			kluczPubliczny = rsa.podajKluczPubliczny();
			System.out.println("Klucz publiczny serwera: " + kluczPubliczny);

			while (true) {
				Socket clientSocket = serverSocket.accept();
				ClientConnectionHandler connectionHandler = new ClientConnectionHandler(clientSocket);
				listaKlientow.add(connectionHandler);
				new Thread(connectionHandler).start();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static void rozeslijWiadomosc(String autor, byte[] wiadomosc) {
		for (ClientConnectionHandler client : listaKlientow) {
			String[] kluczPubliczny = client.podajKluczPubliczny().split(":");
			String odszyfowanaWiadomosc = new String(rsa.odszyfruj(wiadomosc));
			System.out.println("Odszyfrowalem wiadomosc po stronie serwera i jest to :" + odszyfowanaWiadomosc);
			client.wyslijWiadomosc(autor, odszyfowanaWiadomosc, kluczPubliczny);
		}
	}

	public static String podajKluczPublicznySerwera() {
		return "handshake:" + rsa.podajKluczPubliczny();
	}
}
