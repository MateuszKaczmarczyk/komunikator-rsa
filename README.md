# README #

Komunikator RSA to projekt zaliczeniowy na przedmiot "Ochrona danych". Opiera się na komunikacji klient-serwer, a dane są szyfrowane za pomocą RSA.

### Jak uruchomić ###

Aby zacząć pracę z komunikatorem, należy zbudować projekt za pomocą Mavena (mvn clean install), a następnie uruchomić instancję serwera (SocketServer z projektu "serwer"), oraz dowolną liczbę instancji klienta (MainApp z projektu "klient").